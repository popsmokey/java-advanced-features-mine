package JavaAdvancedFeaturesCoding.WeekendOne;

import java.util.Scanner;

public class CalculatorForme implements Program{

    public static Scanner scanner = new Scanner(System.in);

    public void run(String userInput) {
        Shape shape = null;
        if(userInput.equals("1")) {
            System.out.println("Square");
            // Cerem dimensiunea laturii
            // Afisam perimetrul si aria
            shape = new Square();
        } else if(userInput.equals("2")) {
            System.out.println("Rectangle");
            // Cerem lungimea si latimea
            // Afisam perimetrul si aria
            shape = new Rectangle();

        } else if(userInput.equals("3")) {
            System.out.println("Circle");
            // Cerem raza
            // Afisam perimetrul si aria
            shape = new Circle();
        } else
            System.out.println("Wrong input");

        shape.requestPerimeter(scanner);
        System.out.println(shape.area());
        System.out.println(shape.perimeter());
    }

    public String getDescription() {
        return "Press 1 for square, " +
                "2 for rectangle, 3 for circle.";
    }

}

interface Shape {
    double area();
    double perimeter();
    void requestPerimeter(Scanner scanner);
}

class Rectangle implements Shape {

    double l;
    double L;

    public double area() {
        return L*l;
    }

    public double perimeter() {
        return 2*(L+l);
    }

    public void requestPerimeter(Scanner scanner) {
        System.out.println("Introduceti lungimea si latimea");
        this.l = scanner.nextDouble();
        this.L = scanner.nextDouble();
    }
}

class Square implements Shape {

    double l;

    public double area() {
        return l*l;
    }

    public double perimeter() {
        return l*4;
    }

    public void requestPerimeter(Scanner scanner) {
        System.out.println("Introduceti lungimea unei laturi");
        this.l = scanner.nextDouble();
    }
}

class Circle implements Shape {

    double r;
    double PI = Math.PI;

    public double area() {
        return PI*(r*r);
    }

    public double perimeter() {
        return 2*PI*r;
    }

    public void requestPerimeter(Scanner scanner) {
        System.out.println("Introduceti raza");
        this.r = scanner.nextDouble();
    }
}