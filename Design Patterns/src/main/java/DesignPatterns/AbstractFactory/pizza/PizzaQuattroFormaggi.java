package DesignPatterns.AbstractFactory.pizza;

public class PizzaQuattroFormaggi extends Pizza {

    private int size;

    public PizzaQuattroFormaggi(int size){
        this.size = size;
    }

    public String getName() {
        return PizzaType.QUATTROFORMAGGI.toString();
    }

    public String getIngredients() {
        return "4 tipuri de branza, sos rosii";
    }

    public int getSize() {
        return size;
    }
}
